import React, { useEffect } from 'react';

import { NavigationContainer } from '@react-navigation/native';

import SplashScreen from 'react-native-splash-screen';

import { SafeAreaView, StyleSheet } from 'react-native';

import codePush from 'react-native-code-push';

import HomeScreen from './src/screen/Home';

import { checkUpdate } from './src/helpers/checkUpdate';
import { wait } from './src/helpers/wait';

function App(): JSX.Element {
  const handleAppOnReady = () => wait(1000).finally(() => SplashScreen.hide());

  // check new version App
  useEffect(() => {
    checkUpdate().catch(() => {});
  }, []);

  return (
    <NavigationContainer onReady={handleAppOnReady}>
      <SafeAreaView style={styles.container}>
        <HomeScreen />
      </SafeAreaView>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default codePush({
  checkFrequency: __DEV__
    ? codePush.CheckFrequency.MANUAL
    : codePush.CheckFrequency.ON_APP_START,
  installMode: codePush.InstallMode.IMMEDIATE,
})(App);
