import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  loadingContainer: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: 'rgba(40, 41, 61, 1)',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default styles;
