import React, { useState, useEffect, useCallback } from 'react';

import { useFocusEffect } from '@react-navigation/native';

import { View, StatusBar, ActivityIndicator, BackHandler } from 'react-native';

import SystemNavigationBar from 'react-native-system-navigation-bar';

import { WebView, WebViewNavigation } from 'react-native-webview';

import { FACE_CHECK_ID_URL } from '../../constants/urls';

import { runJS } from '../../helpers/updateDOM';

import { wait } from '../../helpers/wait';

import styles from './styles';

function Home(): JSX.Element {
  const [loading, setLoading] = useState(true);
  const [uri, setUri] = useState(FACE_CHECK_ID_URL);

  useFocusEffect(
    useCallback(() => {
      const onBackPress = () => {
        if (uri !== FACE_CHECK_ID_URL) {
          setUri(FACE_CHECK_ID_URL);
          return true;
        }

        return false;
      };

      const backHandler = BackHandler.addEventListener(
        'hardwareBackPress',
        onBackPress,
      );

      return () => backHandler.remove();
    }, [uri]),
  );

  const handleNavigationStateChange = (event: WebViewNavigation) => {
    setUri(event.url);
  };

  const handlePageLoad = () => wait(500).finally(() => setLoading(false));

  useEffect(() => {
    setLoading(true);
  }, [uri]);

  useEffect(() => {
    if (loading) {
      SystemNavigationBar.setNavigationColor('#28293d').catch(() => {});
    } else {
      SystemNavigationBar.setNavigationColor('#1c1c27').catch(() => {});
    }
  }, [loading]);

  return (
    <>
      <StatusBar barStyle="light-content" backgroundColor="#28293d" />
      <WebView
        injectedJavaScript={runJS}
        javaScriptEnabled={true}
        showsVerticalScrollIndicator={false}
        onLoad={handlePageLoad}
        onNavigationStateChange={handleNavigationStateChange}
        source={{ uri }}
        style={styles.container}
      />
      {loading ? (
        <View style={styles.loadingContainer}>
          <ActivityIndicator color="#9a20ae" size="large" />
        </View>
      ) : null}
    </>
  );
}

export default Home;
