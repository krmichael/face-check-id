import {
  startUpdateFlow,
  checkUpdateAvailability,
} from '@gurukumparan/react-native-android-inapp-updates';

export async function checkUpdate() {
  const updateAvailable = await checkUpdateAvailability();

  if (updateAvailable) {
    await startUpdateFlow('flexible');
  }
}
