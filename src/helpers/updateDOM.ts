import { APP_SOURCE_CODE_URL } from '../constants/urls';

export const addDarkMode =
  "document.getElementsByTagName('body')[0].classList.add('dark');";

export const hideSwitchButtonTheme =
  "document.getElementById('menudarkswitch').style.display = 'none';";

export const sideBarChangeBackgroundColor =
  "document.getElementsByClassName('menu__box')[0].style.backgroundColor = '#28293d';";

export const hideAnchorLogo =
  "document.getElementsByClassName('navbar-brand logolink')[0].style.display = 'none';";

export const footerChangeBackgroundColor =
  "document.getElementsByTagName('footer')[0].style.backgroundColor = '#1c1c27';";

export const addSourceCodeEntry = `
  var menu_box = document.getElementsByClassName('menu__box')[0];
  var listItem = document.createElement('li');
  var anchor = document.createElement('a');
  anchor.href = '${APP_SOURCE_CODE_URL}';
  anchor.className = 'menu__item';
  anchor.innerText = 'App (source code)';
  listItem.appendChild(anchor);
  menu_box.appendChild(listItem);
`;

export const runJS = `
  ${addDarkMode}
  ${hideSwitchButtonTheme}
  ${sideBarChangeBackgroundColor}
  ${hideAnchorLogo}
  ${footerChangeBackgroundColor}
  ${addSourceCodeEntry}
  true;
`;
