export const wait = (ms: number) =>
  new Promise((resolve) => setTimeout(() => resolve('ok!'), ms));
